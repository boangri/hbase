import happybase

connection = happybase.Connection('db', 9090)
tablename = 'mytable2'
# connection.create_table(tablename, {'family': dict()})


# connection.create_table(
#     tablename,
#     {'cf1': dict(max_versions=10),
#      'cf2': dict(max_versions=1, block_cache_enabled=False),
#      'cf3': dict(),  # use defaults
#     }
# )

print(connection.tables())

table = connection.table(tablename)
table.put('row-key', {'family:qual1': 'value1', 'family:qual2': 'value2'})
# table.put('key2', {'cf1:q1': 'value1', 'cf2:q2': 'value2'})
for k, data in table.scan():
    print(k, data)
