HAND_NOMINAL_DICT = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'J': 11,
    'T': 10
}

HAND_SUIT_DICT = {
    '♦': 1,
    '♠': 2,
    '♥': 3,
    '♣': 4,
    'd': 1,
    's': 2,
    'h': 3,
    'c': 4
}

OUT_OF_GAME_IN_START_LIST = [
    ['BU', 'SB', 'BB', 'LJ', 'HJ', 'CO'],
    ['BU', 'SB', 'BB', 'HJ', 'CO'],
    ['BU', 'SB', 'BB', 'CO'],
    ['BU', 'SB', 'BB'],
    ['BU', 'BB']
]

STREET_DICT = {
    3: 'flop',
    4: 'turn',
    5: 'river',
    0: 'preflop'
}
