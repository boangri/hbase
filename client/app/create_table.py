import happybase
from functions import *
from utils import hand_list_mapping_string

connection = happybase.Connection('db', 9090)

tablename = f'real1326'
families = {hand_list_mapping_string(key): dict(max_versions=1) for key in all_hands}

try:
    connection.create_table(tablename, families)
except Exception as e:
    print(e)

print(connection.tables())
