from time import time
from functions import *
from utils import hand_list_mapping_string

range_1 = all_hands
range_2 = all_hands


for k, flop in enumerate(flop_type_lst[:10]):
    start_time = time()
    equity_dict_1, _ = equity_calculate_to_dict(range_1, range_2, flop )
    stop_time = time()

    flop_str = ''
    for i in flop:
        flop_str += card_list_mapping_string(i) #ключ

    key = flop_str
    # print(key)
    dic = equity_dict_1
    data = {}
    for k1 in dic.keys():
        for k2 in dic[k1].keys():
            data[k1 + ':' + k2] = "%.3f" % dic[k1][k2]

    # print(f"{data}=data")
    print(f"key={key} k={k}")

    with open('data/%04d_%s.json' % (k, key), 'w') as outfile:
        json.dump(data, outfile)

    print('Took %.3f secs' % (stop_time - start_time))