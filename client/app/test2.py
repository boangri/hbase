import happybase

connection = happybase.Connection('db', 9090)
tablename = 'test-table2'
connection.create_table(tablename, {'family': dict()})
table = connection.table(tablename)

table.put(b'row-key', {b'family:qual1': b'value1',
                       b'family:qual2': b'value2'})

row = table.row(b'row-key')
print(row[b'family:qual1'])  # prints 'value1'

for key, data in table.rows([b'row-key-1', b'row-key-2']):
    print(key, data)  # prints row key and data for each row

for key, data in table.scan(row_prefix=b'row'):
    print(key, data)  # prints 'value1' and 'value2'

row = table.delete(b'row-key')


table.put(b'row-key', {
    b'f1:q1': b'value11', b'f1:q2': b'value12',
    b'f2:q1': b'value21', b'f2:q2': b'value22'})