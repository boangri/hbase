import happybase
from random import random
from time import time

key = '2d2s4h'

connection = happybase.Connection('db', 9090)
N = 1326
tablename = f'tab{N}-1'
families = {'%04d' % i : dict(max_versions=1) for i in range(N)}
try:
    connection.create_table(tablename, families)
except Exception as e:
    print(e)
table = connection.table(tablename)

start_time = time()
data = {}
for i in range(N):
    for j in range(N):
        data['%04d:%04d' % (i, j)] = '%.3f' % random()
#    print('.', end='')
print('Took %.3f secs' % (time() - start_time))
#%%

print(f"{data}=data")

time_start=time()


table.put(key, data)

#for k, data in table.scan():
print('Took %.3f secs' % (time() - start_time))
