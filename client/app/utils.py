from typing import Union
import itertools
# from poker import Range
# import pandas as pd
from constants import HAND_NOMINAL_DICT, HAND_SUIT_DICT


def card_string_mapping_list(hand_nominal: str, hand_suit: str, return_string=False) -> Union[list, str]:
    # Раньше принимал list, сейчас отдельно hand_nominal и hand_suit
    # hand_nominal = card[0]
    # hand_suit = card[1]
    if hand_nominal in HAND_NOMINAL_DICT:
        hand_nominal_out = HAND_NOMINAL_DICT[hand_nominal]
    else:
        hand_nominal_out = int(hand_nominal)

    hand_suit_out = HAND_SUIT_DICT[hand_suit]

    if not return_string:
        return [hand_nominal_out, hand_suit_out]
    else:
        return hand_nominal + hand_suit_out


def hand_string_mapping_list(hand_in):
    # перевод руки в из вида '9d8d' в вид [[8, 1], [9, 1]]
    hand_out = [card_string_mapping_list(hand_in[0:2]), card_string_mapping_list(hand_in[2:4])]
    hand_out.sort()
    return (hand_out)


def card_list_mapping_string(card):
    # отображает карту вида [13,3] в вид строчный 'Kh'
    '''
    нужно отрефакторить по аналогии
    '''
    #print('card', card)
    hand_nominal = card[0]
    hand_suit = card[1]

    if hand_nominal == 14:
        hand_nominal_out = 'A'
    elif hand_nominal == 13:
        hand_nominal_out = 'K'
    elif hand_nominal == 12:
        hand_nominal_out = 'Q'
    elif hand_nominal == 11:
        hand_nominal_out = 'J'
    elif hand_nominal == 10:
        hand_nominal_out = 'T'
    else:
        hand_nominal_out = str(hand_nominal)
    if hand_suit == 1:
        hand_suit_out = 'd'
    elif hand_suit == 2:
        hand_suit_out = 's'
    elif hand_suit == 3:
        hand_suit_out = 'h'
    elif hand_suit == 4:
        hand_suit_out = 'c'

    return (hand_nominal_out + hand_suit_out)


# отображение руки вида [[9, 1], [10, 1]] в  'Td9d'
def hand_list_mapping_string(hand_in):
    hand_out = card_list_mapping_string(hand_in[0]) + card_list_mapping_string(hand_in[1])
    return (hand_out)

deck = [[i, j] for i in range(2, 15) for j in range(1, 5)]
all_hands = list(itertools.combinations(deck, 2))

all_flops = list(itertools.combinations(deck, 3))