import happybase
from time import time

connection = happybase.Connection('db', 9090)
N = 1326
tablename = f'tab{N}-1'
# tablename = 'real1326'
table = connection.table(tablename)

# key = 'key1'
key = '2d2s2h'
start_time = time()
data = table.row(key)
stop_time = time()
print(data)
print('It took %.3f secs' % (stop_time - start_time))