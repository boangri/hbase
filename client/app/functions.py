import subprocess
import json

from time import time
import itertools
from utils import *


def equity_calculate(range_string_1, range_string_2, board):
    '''
    расчет калькулятором
    range_string_1,range_string_2 - строки c руками типа 'Ah4c,Ks2h...'
    board - доска одной строкой вида 'Ah4c3h'
    выводит текстовый json файл, который можно перевести в список
    '''
    # start_time=time.time()
    #result = subprocess.run(["refactored\ps-eval.exe", range_string_1, range_string_2, "--board", board], shell=True, stdout=subprocess.PIPE)
    # print(f"str1 = {range_string_1}")
    # print(f"str2 = {range_string_2}")
    print(f"board: {board}")
    start = time()
    result = subprocess.run(["./calc/ps-eval", range_string_1, range_string_2, "--board", board], stdout=subprocess.PIPE)
    print("Time %.3f" % (time() - start))
    return result

#%%
def equity_calculate_to_dict(range_1, range_2, board):
    '''
    на входе 2 диапазона и борд
    на выходе 2 словаря с эквити каждой руки против каждой
    '''

    lst_for_equity_1 = [hand_list_mapping_string(i) for i in range_1]
    lst_for_equity_2 = [hand_list_mapping_string(i) for i in range_2]

    str_for_equity_1 = ''
    for i in lst_for_equity_1:
        str_for_equity_1 += i
        if i != lst_for_equity_1[-1]:
            str_for_equity_1 += ';'

    str_for_equity_2 = ''
    for i in lst_for_equity_2:
        str_for_equity_2 += i
        if i != lst_for_equity_2[-1]:
            str_for_equity_2 += ';'

    board_calc = ''
    for i in board:
        board_calc += card_list_mapping_string(i)

    x = equity_calculate(str_for_equity_1, str_for_equity_2, board=board_calc)

    equity_lst = json.loads(x.stdout)

    # создания словаря c эквити каждая против каждой руки
    equity_dict = {}
    for i in lst_for_equity_1:
        equity_dict[i] = {}

    for j in equity_lst:
        if j['games'] == '0':
            continue
        else:
            hand_1 = list(j.keys())[1]
            hand_2 = list(j.keys())[2]
            equity = j[hand_1]['equity']
            equity_dict[hand_1][hand_2] = float(equity[:-1]) / 100

    # для 2го игрока то же самое для удобства
    equity_dict_for_2 = {}
    for i in lst_for_equity_2:
        equity_dict_for_2[i] = {}

    for j in equity_lst:
        if j['games'] == '0':
            continue
        else:
            hand_1 = list(j.keys())[1]
            hand_2 = list(j.keys())[2]
            equity = j[hand_2]['equity']
            equity_dict_for_2[hand_2][hand_1] = float(equity[:-1]) / 100

    return equity_dict, equity_dict_for_2
#%%
def flop_mapping(flop):
    x = flop[0]
    y = flop[1]
    z = flop[2]
    #print('flop', flop)
    #print('xyz',x,y,z)
    case = [-999, -999]
    lst_suit=[x[1], y[1], z[1]]
    if x[0] < y[0] < z[0]:  # номиналы разные
        if x[1] != y[1] != z[1] != x[1]:  # все разные масти
            lst_suit_new = [1, 2, 3]
            case = '1_1'
        elif x[1] == y[1] != z[1]:
            lst_suit_new = [1, 1, 2]
            case = '1_2'
        elif x[1] != y[1] == z[1]:
            lst_suit_new = [1, 2, 2]
            case = '1_3'
        elif x[1] != y[1] != z[1] == x[1]:
            lst_suit_new = [1, 2, 1]
            case = '1_4'
        elif x[1] == y[1] == z[1] == x[1]:
            lst_suit_new = [1, 1, 1]
            case = '1_5'
        else:
            lst_suit_new = [-999, -999, -999]

    elif x[0] == y[0] < z[0]:  # номиналы разные
        #print('flop', flop)
        #print(x[0] , y[0] , z[0])
        if x[1] != y[1] != z[1] != x[1]:  # все разные масти
            lst_suit_new = [1, 2, 3]
            case = '2_1'
        elif x[1] == y[1] != z[1]:
            lst_suit_new = [1, 1, 2]
            case = '2_2'
        elif x[1] != y[1] == z[1]:
            lst_suit_new = [1, 2, 2]
            case = '2_3'
        elif x[1] != y[1] != z[1] == x[1]:
            lst_suit_new = [1, 2, 1]
            case = '2_4'
        elif x[1] == y[1] == z[1] == x[1]:
            lst_suit_new = [1, 1, 1]
            case = '2_5'
        else:
            lst_suit_new = [-999, -999, -999]

    elif x[0] < y[0] == z[0]:  # номиналы разные
        if x[1] != y[1] != z[1] != x[1]:  # все разные масти
            lst_suit_new = [1, 2, 3]
            case = '3_1'
        elif x[1] == y[1] != z[1]:
            lst_suit_new = [1, 1, 2]
            case = '3_2'
        elif x[1] != y[1] == z[1]:
            lst_suit_new = [1, 2, 2]
            case = '3_3'
        elif x[1] != y[1] != z[1] == x[1]:
            lst_suit_new = [1, 2, 1]
            case = '3_4'
        elif x[1] == y[1] == z[1] == x[1]:
            lst_suit_new = [1, 1, 1]
            case = '3_5'
        else:
            lst_suit_new = [-999, -999, -999]

    elif x[0] == y[0] == z[0]:  # номиналы разные
        if x[1] != y[1] != z[1] != x[1]:  # все разные масти
            lst_suit_new = [1, 2, 3]
            case = '4_1'
        elif x[1] == y[1] != z[1]:
            lst_suit_new = [1, 1, 2]
            case = '4_2'
        elif x[1] != y[1] == z[1]:
            lst_suit_new = [1, 2, 2]
            case = '4_3'
        elif x[1] != y[1] != z[1] == x[1]:
            lst_suit_new = [1, 2, 1]
            case = '4_4'
        elif x[1] == y[1] == z[1] == x[1]:
            lst_suit_new = [1, 1, 1]
            case = '4_5'
        else:
            lst_suit_new = [-999, -999, -999]
    new_flop = [[x[0], lst_suit_new[0]], [y[0], lst_suit_new[1]], [z[0], lst_suit_new[2]]]
    return (new_flop, lst_suit, lst_suit_new, case)  #suit_dict_mapping, suit_dict_mapping_reverse)


#%%
dict_count_case={}
flop_type_lst = []
for flop in all_flops:
    new_flop, lst_suit, lst_suit_new, case = flop_mapping(flop=flop)
    if new_flop not in flop_type_lst:
        flop_type_lst += [new_flop]
    if case not in dict_count_case.keys():
        dict_count_case[case]=1
    else:
        dict_count_case[case]+=1

# print(flop_type_lst)
print(f"flop_type_list len={len(flop_type_lst)}")