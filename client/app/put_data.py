import happybase
from time import time
from random import random
from functions import *
from utils import hand_list_mapping_string
import os

# connection = happybase.Connection('db', 9090)

tablename = f'real1326'

# table = connection.table(tablename)

for filename in sorted(os.listdir("data")):
    k, rest = filename.split('_')
    key, _ = rest.split('.')
    print(filename, k, key)

    with open('data/' + filename) as json_file:
        data = json.load(json_file)

    start_time = time()
    connection = happybase.Connection('db', 9090)
    table = connection.table(tablename)
    table.put(key, data)
    connection.close()
    print('Took %.3f secs' % (time() - start_time))
