import happybase
from time import time
from random import random
from functions import *
from utils import hand_list_mapping_string

range_1 = all_hands
range_2 = all_hands

connection = happybase.Connection('db', 9090)

# tablename = f'real1326'
# families = {hand_list_mapping_string(key).lower(): dict(max_versions=1) for key in all_hands}

N = 1326
tablename = f'tab{N}-1'
families = {'%04d' % i : dict(max_versions=1) for i in range(N)}
try:
    connection.create_table(tablename, families)
except Exception as e:
    print(e)

table = connection.table(tablename)

for flop in flop_type_lst[:1]:
    # equity_dict_1, _ = equity_calculate_to_dict(range_1, range_2, flop )
    flop_str = ''
    for i in flop:
        flop_str += card_list_mapping_string(i) #ключ
    '''
нужно записать ключ и данные. Ключи одинаковые в каждой итерации
если записываем числа в виде строки, то длинные числа можно сократить до 3х знаков
    '''
    key = flop_str
    # dic = equity_dict_1
    # del dic
    # del equity_dict_1
    data = {}
    # for k1 in dic.keys():
    #     for k2 in dic[k1].keys():
    #         data[k1.lower() + ':' + k2.lower()] = "%.0f" % (1000 * dic[k1][k2])
    for i in range(N):
        for j in range(N):
            data['%04d:%04d' % (i, j)] = '%.3f' % random()
    print(f"{data}=data")
    print(f"key={key}")

    start_time = time()
    table.put(key.lower(), data)
    print('Took %.3f secs' % (time() - start_time))
