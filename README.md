# Тестовый пример использования HBase

## Установка

Сборка/загрузка образов контейнеров

под `dc` понимается алиас для `docker-compose` (он уже создан для пользователя `feniks`)
```shell
dc up -d --build
```
Проверка состояния контейнеров:
```shell
dc ps
```
Запуск контейнеров (ключ --build можно не указывать при повторных запусках):
```shell
dc up -d 
```
Приложение состоит из 2-х контенеров - для базы и клиента.

Рекомендуется убедиться что оба контейнера в состоянии `running` (в столбце `STATUS`)

Останов:
```shell
dc down
```
## Тестовые примеры 

Тестовые примеры программ на языке Python находятся в каталоге `client/app`

Программы запускаются из контейнера `client`:

```shell
dc exec client bash
feliks@111be9f7a378:~/app$ python test.py
[b'mytable']
b'row-key' {b'family:qual1': b'value1', b'family:qual2': b'value2'}
```

Тестовая программа `test.py`:

```python
import happybase

connection = happybase.Connection('db', 9090) 
tablename = 'mytable2'
connection.create_table(tablename, {'family': dict()})

print(connection.tables())

table = connection.table(tablename)
table.put('row-key', {'family:qual1': 'value1', 'family:qual2': 'value2'})
for k, data in table.scan():
    print(k, data)
```
В программе создается таблица, выводится список таблиц. 
Далее заносятся тестовые данные и считываются с выводом на печать.

При повторном запуске программы надо либо изменить имя таблицы, либо закомментировать ее создание
(иначе будет ошибка при попытке создания таблицы)

Программа `test2.py` работает аналогично.